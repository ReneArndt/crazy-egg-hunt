﻿using UnityEngine;
using System.Collections;

public class EggClass : MonoBehaviour {

	[SerializeField]
	private GameObject splashReference;
	private Vector3 randomPos;
	private GUIText scoreReference;

	// Use this for initialization
	void Start () {
		randomPos = new Vector3(Random.Range(-1, 1), Random.Range(0.3f, 0.7f), Random.Range(-6.5f, -7.5f));
		scoreReference = GameObject.Find("Score").GetComponent<GUIText>();
	}
	
	// Update is called once per frame
	void Update () {
		if (gameObject.transform.position.y < -36)
		{
			Destroy(gameObject);
		}
	}

	void OnCollisionEnter(Collision other)
	{
		if(other.gameObject.name == "Line")
		{
			Camera.main.GetComponent<AudioSource>().Play();
			Destroy(gameObject);
			
			Instantiate(splashReference, randomPos, transform.rotation);
			
			/* Update Score */
			
			scoreReference.text = (int.Parse(scoreReference.text) + 1).ToString();
		}
	}
}
